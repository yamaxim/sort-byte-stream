<?php
error_reporting(0);

use Symfony\Component\Console\Application;
use Maaaxim\Command\HeapSortCommand;
use Maaaxim\Command\CountSortCommand;
use Maaaxim\Command\DeikstraAlgorythmCommand;

require("./vendor/autoload.php");

$app = new Application('Welcome', "v1.0.0");
$app->add(new HeapSortCommand());
$app->add(new DeikstraAlgorythmCommand());
$app->add(new CountSortCommand());
$app->run();