<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 11/8/18
 * Time: 1:23 AM
 */

namespace Maaaxim\Command;

use Maaaxim\Service\Deikstra;
use Maaaxim\Service\Graph;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class HeapSortCommand
 * @package Maaaxim\Command
 */
class DeikstraAlgorythmCommand extends Command
{
    /**
     * HeapSortCommand constructor.
     * @param null|string $name
     */
    public function __construct(?string $name = null)
    {
        parent::__construct($name);
    }

    /**
     * Configure
     */
    public function configure()
    {
        $this->setName('deikstra')
            ->setDescription("This console run command")
            ->addArgument('file', InputArgument::REQUIRED . 'Your file');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $input->getArgument('file');
        if(!$file || !file_exists($file)){
            throw new InvalidArgumentException("No file!");
        }

        $graph = new Graph($file);
        $service = new Deikstra($graph);
        $service->job();
    }
}