<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 11/8/18
 * Time: 1:23 AM
 */

namespace Maaaxim\Command;

use Maaaxim\Service\HeapSort;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class CountSortCommand
 * @package Maaaxim\Command
 */
class CountSortCommand extends Command
{
    /**
     * Configure
     */
    public function configure()
    {
        $this->setName('sort-count')
            ->setDescription("This console run command")
            ->addArgument('file', InputArgument::REQUIRED . 'Your file');
    }
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $input->getArgument('file');
        if(!$file || !file_exists($file)){
            throw new InvalidArgumentException("No file!");
        }

        // Сгенерируем массив с количеством вхождений каждого байта
        $resourse = fopen($file, "rb");
        $byteCount = [];
        while (!feof($resourse)) {
            $contents = fread($resourse, 1);
            $byteCount[ord($contents)]++;
        }
        fclose($resourse);

        // Запишем в файл биты в порядке ключей
        $file = "some.txt";
        $resourse = fopen($file, "w");
        for($byte = 0; $byte <= sizeof($byteCount); $byte++){
            for($i = 0; $i < $byteCount[$byte]; $i++){
                fwrite($resourse, $byte);
            }
        }
        fclose($resourse);
    }
}