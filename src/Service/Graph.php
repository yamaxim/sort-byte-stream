<?php
namespace Maaaxim\Service;

class Graph
{
    protected $nodes = [];

    /**
     * Graph constructor.
     * @param $jsonFile
     */
    public function __construct($jsonFile)
    {
        $fileContents = file_get_contents($jsonFile);
        $matrix = json_decode($fileContents);
        foreach($matrix as $key => $node){

            $connections = [];

            // Найдем рёбра и стоимость
            $bounds = array_filter($node, function ($value, $key){
                if($value > 0){
                    return [$key, $value];
                }
            }, ARRAY_FILTER_USE_BOTH);

            foreach($matrix as $subKey => $subNode){

                // Без цикличности
                if($key === $subKey){
                    continue;
                }

                // Найдем ид связанной ноды
                foreach ($bounds as $kBound => $bound){
                    if($subNode[$kBound] !== 0){
                        $connections[$subKey] = $bound;
                    }
                }
            }

            $this->nodes[] = new Node($key, $connections);
        }
    }

    /**
     * @param int $curNodeId
     * @param int $curBoundCost
     * @param int $prevNodeId
     */
    public function setWeights($curNodeId = 0, $curBoundCost = 0, $prevNodeId = 0)
    {

        $node = $this->nodes[$curNodeId];
        $prevNode = $this->nodes[$prevNodeId];
        $prevNodeCost = (int) $prevNode->getCost();

        $connections = $node->getConnections();
        $hadCost = $node->getCost();

        // проставляем цену = цена предыдущей ноды + цена ведущего к текущей ноде ребра
        $cost = $prevNodeCost + (int) $curBoundCost;
        if($hadCost === false){
            $node->setCost($cost);
        } else {

            // если цена была установлена, сравним её с новой. если новая меньше - перепишем
            if($cost < $hadCost){
                $node->setCost($cost);
                $node->setUnVisited();
            }
        }

        // Не ветвимся, если были тут
        if(!$node->isVisited()){

            $node->setVisited();

            // передаем связанным нодам текущую цену
            asort($connections);
            foreach($connections as $nodeId => $boundCost){
                $this->setWeights($nodeId, $boundCost, $curNodeId);
            }
        }
    }

    /**
     * @return array
     */
    public function getNodes(): array
    {
        return $this->nodes;
    }
}