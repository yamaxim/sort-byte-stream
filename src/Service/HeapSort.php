<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 11/8/18
 * Time: 12:41 AM
 */

namespace Maaaxim\Service;

/**
 * Сортирует на месте, то есть требует всего O(1) дополнительной памяти
 * https://ru.wikipedia.org/wiki/%D0%9F%D0%B8%D1%80%D0%B0%D0%BC%D0%B8%D0%B4%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0
 *
 * Class HeapSort
 * @package Maaaxim
 */
class HeapSort
{
    /**
     * @param string $file
     */
    public function proceed(string $file): void
    {
        $resourse = fopen($file, "rb");

        $data = [];
        while (!feof($resourse)) {
            $contents = fread($resourse, 1);
            $data[] = ord($contents);
        }
        fclose($resourse);

        $time_start = microtime(true);

        self::sort($data);

        $time_end = microtime(true);

        $execution_time = ($time_end - $time_start);
        echo 'Total Execution Time: '.$execution_time.' seconds' . PHP_EOL;
    }

    /**
     * @param array $array
     */
    public static function sort(array &$array): void
    {
        $n = count($array);
        for($i = ($n / 2) - 1; $i >= 0; $i--) {             // построение пирамиды
            self::sink($array, $i, $n - 1);
        }

        for($i = $n - 1; $i > 0; $i--) {
            $t = $array[$i];                                // меняем первый элемент в пирамиде на последний
            $array[$i] = $array[0];
            $array[0] = $t;
            self::sink($array, 0, $i - 1);        // восстанавливаем пирамидальность
        }
    }

    /**
     * @param array $array
     * @param $k
     * @param $n
     */
    private static function sink(array &$array, $k, $n): void
    {
        $e = $array[$k];
        while($k <= $n / 2) {                               // цикл выполняется пока есть потомки
            $j = $k * 2;                                    // индекс первого потомка
            if($j < $n && $array[$j] < $array[$j + 1]) {    // выбор наибольшего потомка
                $j++;
            }
            if($e >= $array[$j]) {
                break;
            }
            $array[$k] = $array[$j];                        // двигаем потомка на уровень выше
            $k = $j;
        }
        $array[$k] = $e;
    }
}