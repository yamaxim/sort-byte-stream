<?php

namespace Maaaxim\Service;

class Node
{
    protected $id;
    protected $visited = false;
    protected $cost = false; // infinity

    /**
     * @var array Of arrays [Node id => Cost]
     */
    protected $connections = [];

    /**
     * Node constructor.
     * @param int $id
     * @param array $connections
     */
    public function __construct(
        int $id,
        array $connections
    )
    {
        $this->id = $id;
        $this->connections = $connections;
    }

    /**
     * @return bool
     */
    public function isVisited(): bool
    {
        return $this->visited;
    }

    /**
     * Set visited = true
     */
    public function setVisited(): void
    {
        $this->visited = true;
    }

    /**
     * Set visited = true
     */
    public function setUnVisited(): void
    {
        $this->visited = false;
    }

    /**
     * @return array
     */
    public function getConnections(): array
    {
        return $this->connections;
    }

    /**
     * @return bool
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param bool $cost
     */
    public function setCost($cost): void
    {
        $this->cost = $cost;
    }
}